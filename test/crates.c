#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include "matrix.h"
#include "shader.h"
#include "vec.h"
#include "texture.h"
#include "light.h"
#include "material.h"

#define CUBE_COUNT 69

#define PI 3.14159f
#define RAD2DEG(x) x*180.0f/PI
#define DEG2RAD(x) x*PI/180.0f

#define WIDTH 1920
#define HEIGHT 1080

void glfw_error_callback(int error, const char* desc) {
	fprintf(stderr, "GLFW_ERROR: %s\n", desc);
}

	void GLAPIENTRY
MessageCallback( GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam )
{
	fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
			( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
			type, severity, message );
}

int main() {
	if (!glfwInit()) {
		printf("ERROR: Could not init glfw\n");
		return -1;
	}
	glfwSetErrorCallback(glfw_error_callback);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	GLFWwindow *win = glfwCreateWindow(WIDTH, HEIGHT, "Vengine", NULL, NULL);
	if (!win) {
		printf("ERROR: Could not create window\n");
		return -1;
	}
	glfwMakeContextCurrent(win);
	gladLoadGL();
	glfwSwapInterval(1);
	glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(win, WIDTH/2, HEIGHT/2); 
	glfwSetInputMode(win, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);

	srand(time(NULL));

	veng_shader shader;
	shader = veng_shader_init("vert.glsl", "frag.glsl");
	if (shader == 0) {
		printf("Exiting\n");
		return -1;
	}

	char default_tex_raw[] = {255, 255, 255};
	veng_tex default_tex;
	veng_gen_tex_raw(default_tex_raw, 1, 1, 3, &default_tex, false);
	veng_bind_tex(&default_tex, 0);

	float vertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
	  	 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
	};

	vec3 positions[CUBE_COUNT];
	vec3 rotations[CUBE_COUNT];

	int range = CUBE_COUNT/3;

	for (int i = 0; i < CUBE_COUNT; i++) {
		positions[i].x = rand()%range - range/2;
		positions[i].y = rand()%range - range/2;
		positions[i].z = rand()%range - range/2;
		rotations[i].x = (float)(rand()%20-10)*PI/5.0f;
		rotations[i].y = (float)(rand()%20-10)*PI/5.0f;
		rotations[i].z = (float)(rand()%20-10)*PI/5.0f;
	}

	vec3 scale = {1, 1, 1};

	vec3 lightscale = {0.2, 0.2, 0.2};

	vec3 light_diffuse = {3, 3, 3};
	vec3 light_specular = {3, 3, 3};
	vec3 light_ambient = {0, 0, 0};
	veng_point_light lights[10];

	veng_spot_light flashlight;
	flashlight.inner = cosf(DEG2RAD(12.0f));
	flashlight.outer = cosf(DEG2RAD(15.0f));
	flashlight.diffuse_color = light_diffuse;
	flashlight.specular_color = light_specular;
	flashlight.ambient_color = light_ambient;

	for (int i = 0; i < 10; i++) {
		lights[i].pos.x = rand()%range - range/2;
		lights[i].pos.y = rand()%range - range/2;
		lights[i].pos.z = rand()%range - range/2;
		lights[i].diffuse_color = light_diffuse;
		lights[i].specular_color = light_specular;
		lights[i].ambient_color = light_ambient;
	}

	float gamma_inv = 1.0/2.2;

	veng_material mat = {
		(vec3){1, 1, 1},
		(vec3){1, 1, 1},
		1,
		2,
		32.0f
	};

	veng_material light_mat = {
		(vec3){1, 1, 1},
		(vec3){1, 1, 1},
		0,
		0,
		1.0f
	};

	vec3 campos = {0, 0, 3};
	vec3 camfront = {0, 0, -1};
	vec3 camright = {1, 0, 0};
	vec3 camup = {0, 1, 0};

	float yaw = 0, pitch = 0;
	double cur_mouse_x, cur_mouse_y;
	float sens_x = 0.1, sens_y = -0.1;
	float cam_speed = 0.1f;

	mat4 view;
	mat4 proj;

	mat4 model_trans;
	mat4 model_rot;
	mat4 model_scale;
	mat4 model;

	uint32_t vbo, vao;
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glBindAttribLocation(shader, 1, "pos");
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(3*sizeof(float)));
	glEnableVertexAttribArray(0);
	glBindAttribLocation(shader, 0, "normal");
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)(6*sizeof(float)));
	glEnableVertexAttribArray(2);
	glBindAttribLocation(shader, 2, "tex_coord");

	glLinkProgram(shader);

	veng_tex diffuse_tex;
	veng_gen_tex("test/textures/crate_diffuse.png", 3, &diffuse_tex, true);

	veng_tex specular_tex;
	veng_gen_tex("test/textures/crate_specular.png", 3, &specular_tex, false);


	veng_persp_mat4(0.1, 100.0f, PI/3.0f, (float)WIDTH/(float)HEIGHT, proj);


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_DEBUG_OUTPUT);

	glDebugMessageCallback(MessageCallback, 0);

	float theta = 0;

	while (!glfwWindowShouldClose(win)) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glfwGetCursorPos(win, &cur_mouse_x, &cur_mouse_y);

		veng_shader_bind(shader);

		pitch += ((float)cur_mouse_y-(float)(HEIGHT/2)) * sens_y;
		yaw += ((float)cur_mouse_x-(float)(WIDTH/2.)) * sens_x;
		if (pitch > 80.0f) {
			pitch = 80.0f;
		} else if (pitch < -80.0f) {
			pitch = -80.0f;
		}
		if (yaw > 360.0f) {
			yaw = yaw - 360.0f;
		} else if (yaw < -360.0f) {
			yaw = yaw + 360.0f;
		}

		glfwSetCursorPos(win, (float)WIDTH/2.0f, (float)HEIGHT/2.0f);
		camfront.x = cosf(DEG2RAD(yaw)) * cosf(DEG2RAD(pitch));
		camfront.y = sinf(DEG2RAD(pitch));
		camfront.z = sinf(DEG2RAD(yaw)) * cosf(DEG2RAD(pitch));

		camfront = veng_normalise_vec3(camfront);
		camright = veng_cross_vec3(camfront, camup);
		camright = veng_normalise_vec3(camright);

		if (glfwGetKey(win, GLFW_KEY_D) == GLFW_PRESS) {
			campos = veng_add_vec3(campos,
					veng_multip_vec3_float(camright, cam_speed));
		}
		if (glfwGetKey(win, GLFW_KEY_A) == GLFW_PRESS) {
			campos = veng_add_vec3(campos,
					veng_multip_vec3_float(camright, -cam_speed));
		}
		if (glfwGetKey(win, GLFW_KEY_W) == GLFW_PRESS) {
			campos = veng_add_vec3(campos,
					veng_multip_vec3_float(camfront, cam_speed));
		}
		if (glfwGetKey(win, GLFW_KEY_S) == GLFW_PRESS) {
			campos = veng_add_vec3(campos,
					veng_multip_vec3_float(camfront, -cam_speed));
		}

		theta += 0.04;
		if (theta == 2*3.1415) {
			theta = 0;
		}

		flashlight.dir = veng_multip_vec3_float(camfront, -1);
		flashlight.pos = campos;

		veng_lookat_mat4(campos, camfront, view);
		veng_shader_uniform_mat4(shader, "view_mat", (float*)view);

		veng_shader_uniform_mat4(shader, "proj_mat", (float*)proj);

		veng_shader_uniform_i(shader, "lit", 1);

		veng_shader_uniform_vec3(shader, "cam_pos", (float*)&campos);

		veng_shader_uniform_material(shader, "mat", &mat);

		veng_shader_uniform_f(shader, "gamma_inv", gamma_inv);

		veng_shader_uniform_spot_light(shader, "spot_light", &flashlight);

		veng_bind_tex(&default_tex, 0);
		veng_bind_tex(&diffuse_tex, 1);
		veng_bind_tex(&specular_tex, 2);

		for (int i = 0; i < CUBE_COUNT; i++) {
			char name[32];
			for (int j = 0; j < 10; j++) {
				sprintf(name, "point_lights[%d]", j);
				veng_shader_uniform_point_light(shader,
						name,
						&lights[j]);
			}
			veng_translate_mat4(positions[i], model_trans);
			veng_rot_mat4(rotations[i], model_rot);
			veng_scale_mat4(scale, model_scale);
			veng_multip_mat4(model_rot, model_scale, model);
			veng_multip_mat4(model_trans, model, model);
			veng_shader_uniform_mat4(shader, "model_mat", (float*)model);

			glBindVertexArray(vao);
			veng_shader_bind(shader);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		for (int i = 0; i < 10; i++) {
			veng_translate_mat4((vec3)lights[i].pos, model_trans);
			veng_scale_mat4(lightscale, model_scale);
			veng_multip_mat4(model_scale, model_trans, model);
			veng_shader_uniform_mat4(shader, "model_mat", (float*)model);

			veng_shader_uniform_i(shader, "lit", 0);
			veng_shader_uniform_material(shader, "mat", &light_mat);
			glBindVertexArray(vao);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		glfwSwapBuffers(win);
		glfwPollEvents();
	}
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glfwTerminate();
	return 0;
}
