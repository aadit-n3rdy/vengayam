project "crates"
	kind "WindowedApp"
	language "C"
	files { "crates.c" }
	includedirs { "../vengayam/include/", "../extern/glfw/include", "../extern/glad/include" }
	links { "vengayam", "glfw", "glad" }
	filter "configurations:debug"
		buildoptions { "-fno-omit-frame-pointer" }
		buildoptions { "-fsanitize=undefined,address" }
		linkoptions { "-fsanitize=undefined,address" }
	filter {}
	configuration { "linux" }
		linkoptions { "-lrt -lm -ldl -lX11 -lpthread -lxcb -lXau -lXdmcp" }
