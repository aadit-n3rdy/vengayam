#version 330 core
out vec4 frag_color;

in vec4 frag_pos;
in vec3 frag_normal;
in vec2 frag_tex_coord;

struct point_light_t {
	vec3 pos;
	vec3 diffuse_color;
	vec3 ambient_color;
	vec3 specular_color;
};

struct spot_light_t {
	vec3 pos;
	vec3 dir;
	float inner;
	float outer;
	vec3 diffuse_color;
	vec3 ambient_color;
	vec3 specular_color;
};

struct material_t {
	vec3 diffuse;
	vec3 specular;
	sampler2D diffuse_tex;
	sampler2D specular_tex;
	float shininess;
};

uniform int lit;
uniform vec3 cam_pos;
uniform point_light_t point_lights[10];
uniform spot_light_t spot_light;
uniform material_t mat;
uniform float gamma_inv;

vec3 lighting_point_light(struct point_light_t light, vec3 view_dir, 
		vec3 obj_diffuse, 
		vec3 obj_specular, 
		float shine) {
	vec3 light_dir = light.pos - frag_pos.xyz;
	float attenuate = 1.0/(dot(light_dir ,light_dir));
	light_dir = normalize(light_dir);
	vec3 halfway_dir = (light_dir + view_dir)/2.0f;
	vec3 ambient = light.ambient_color * obj_diffuse;
	vec3 diffuse = max(dot(frag_normal, light_dir), 0.0) * light.diffuse_color * obj_diffuse;
	vec3 specular;
	if (diffuse == vec3(0.0)) {
		specular = vec3(0.0);
	} else {
		specular = pow(max(dot(frag_normal, halfway_dir), 0.0), shine) *
			light.specular_color *
			obj_specular;
	}
	return (ambient + diffuse + specular) * attenuate;
}

vec3 lighting_spot_light(struct spot_light_t light, vec3 view_dir,
		vec3 obj_diffuse,
		vec3 obj_specular,
		float shine) {
	vec3 light_dir = light.pos - frag_pos.xyz;
	float attenuate = 1.0/(dot(light_dir, light_dir));
	light_dir = normalize(light_dir);
	vec3 halfway_dir = (light_dir + view_dir)/2.0f;
	float cost = dot(light_dir, light.dir);
	float strength = 0.0f;
	if (cost > light.inner) {
		strength = 1.0f;
	} else if (cost > light.outer) {
		strength = (cost - light.outer)/(light.inner-light.outer);
	}
	if (strength != 0.0f) {
		vec3 ambient = light.ambient_color * obj_diffuse;
		vec3 diffuse = max(dot(frag_normal, light_dir), 0.0) * light.diffuse_color * obj_diffuse;
		vec3 specular;
		if (diffuse == vec3(0.0)) {
			specular = vec3(0.0);
		} else {
			specular = pow(max(dot(frag_normal, halfway_dir), 0.0), shine) *
				light.specular_color *
				obj_specular;
		}
		return (ambient + diffuse + specular) * attenuate * strength;
	} else {
		return vec3(0);
	}

}

void main()
{
	if (lit > 0) {
		vec3 obj_diffuse = texture(mat.diffuse_tex, frag_tex_coord).xyz * 
			mat.diffuse;
		vec3 obj_specular =  texture(mat.specular_tex, frag_tex_coord).xyz * 
			mat.specular;
		vec3 view_dir = normalize(cam_pos - frag_pos.xyz);

		frag_color.xyz = vec3(0);
		frag_color.w = 1.0;
		
		int i = 0;	
		for(i = 0; i < 10; i++) {
			frag_color += vec4(lighting_point_light(point_lights[i], view_dir,
			obj_diffuse,
			obj_specular,
			mat.shininess), 0.0);
		}
		frag_color.rgb += lighting_spot_light(spot_light, view_dir,
				obj_diffuse,
				obj_specular,
				mat.shininess);
		frag_color.rgb = pow(frag_color.rgb, vec3(gamma_inv));
	} else {
		frag_color.rgb = pow(texture(mat.diffuse_tex, frag_tex_coord).xyz * 
			mat.diffuse, vec3(gamma_inv));
		frag_color.w = 1.0;
	}
}

