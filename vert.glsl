#version 330 core
/*layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coord;*/
in vec3 pos;
in vec3 normal;
in vec2 tex_coord;

uniform mat4 model_mat;
uniform mat4 view_mat;
uniform mat4 proj_mat;

out vec4 frag_pos;
out vec3 frag_normal;
out vec2 frag_tex_coord;

void main()
{
	frag_pos = model_mat * vec4(pos, 1.0);
	frag_normal = normalize(mat3(transpose(inverse(model_mat))) * normal);
	gl_Position = proj_mat * view_mat * model_mat * vec4(pos, 1.0);
	frag_tex_coord = tex_coord;
}
