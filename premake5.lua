workspace "vengayam"
	configurations { "debug", "release" }
	buildoptions { "--std=c99", "-Wall" }
	toolset "clang"
	filter "configurations:debug"
		defines { "DEBUG" }
		symbols "On"
	filter "configurations:release"
		defines { "NDEBUG" }
		optimize "On"


include "extern/glad/glad.lua"
include	"extern/glfw/glfw.lua"
include	"vengayam/vengayam.lua"
include	"test/test.lua"
