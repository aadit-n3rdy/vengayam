#ifndef VENG_LIGHT_H
#define VENG_LIGHT_H

#include "vec.h"

typedef struct veng_point_light {
	vec3 pos;
	vec3 diffuse_color;
	vec3 ambient_color;
	vec3 specular_color;
} veng_point_light;

typedef struct veng_spot_light {
	vec3 pos;
	vec3 dir;
	float inner;
	float outer;
	vec3 diffuse_color;
	vec3 ambient_color;
	vec3 specular_color;

} veng_spot_light;

#endif
