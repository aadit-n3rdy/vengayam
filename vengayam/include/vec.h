#ifndef VENG_VEC_H
#define VENG_VEC_H

typedef struct vec3 {
	float x;
	float y;
	float z;
} __attribute__((packed)) vec3;

typedef struct vec4 {
	float x;
	float y;
	float z;
	float w;
} __attribute__((packed)) vec4;

vec3 veng_cross_vec3(vec3 v1, vec3 v2);

float veng_dot_vec3(vec3 v1, vec3 v2);

vec3 veng_normalise_vec3(vec3 v);

vec3 veng_multip_vec3_float(vec3 v, float f);

vec3 veng_add_vec3(vec3 v1, vec3 v2);

vec4 veng_normalise_vec4(vec4 v);

#endif
