#ifndef VENG_MATERIAL_H
#define VENG_MATERIAL_H

#include "vec.h"

typedef struct veng_material {
	vec3 diffuse;
	vec3 specular;
	int diffuse_tex_slot;
	int specular_tex_slot;
	float shininess;
} veng_material;

#endif
