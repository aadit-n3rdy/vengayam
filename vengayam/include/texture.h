#ifndef VENG_TEXTURE_H
#define VENG_TEXTURE_H

#include <stdint.h>
#include <stdbool.h>

typedef struct veng_tex {
	uint32_t id;
	int width;
	int height;
	int channels;
} veng_tex;

int veng_gen_tex(const char* path, int channels, veng_tex *tex, bool srgb);
int veng_gen_tex_raw(const char *data, int width, int height, int channels, veng_tex *tex, bool srgb);
int veng_bind_tex(veng_tex *tex, uint32_t slot);

#endif
