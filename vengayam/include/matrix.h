#ifndef VENG_MATRIX_H
#define VENG_MATRIX_H

#include "vec.h"

typedef float mat4[4][4];

int veng_multip_mat4 (mat4 m1, mat4 m2, mat4 dest);

vec4 veng_multip_mat4_vec4 (mat4 m, vec4 v);

int veng_translate_mat4 (vec3 v, mat4 dest);

int veng_scale_mat4 (vec3 v, mat4 dest);

int veng_persp_mat4 (float near, float far, float fov, float ratio, mat4 dest);

int veng_rot_mat4 (vec3 v, mat4 dest);

int veng_print_mat4 (mat4 m);

int veng_lookat_mat4 (vec3 cam, vec3 front, mat4 result);

#endif

