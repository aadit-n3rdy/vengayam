#ifndef VENG_SHADER_H
#define VENG_SHADER_H

#include "light.h"
#include "material.h"

#include <stdint.h>

typedef uint32_t veng_shader;

uint32_t veng_compile_shader(const char *fname, uint32_t shader_type);

veng_shader veng_shader_init(const char *vert_fname, const char *frag_fname);

int veng_shader_uniform_mat4(veng_shader shader, const char* name, float *val);

int veng_shader_uniform_vec3(veng_shader shader, const char* name, float *val);

int veng_shader_uniform_ui(veng_shader shader, const char* name, uint32_t val);

int veng_shader_uniform_i(veng_shader shader, const char* name, int32_t val);

void veng_shader_bind(veng_shader shader);

int veng_shader_uniform_f(veng_shader shader, const char* name, float val);

int veng_shader_uniform_point_light(veng_shader shader, const char* name, 
		veng_point_light *light);

int veng_shader_uniform_spot_light(veng_shader shader, const char *name, 
		veng_spot_light *light);

int veng_shader_uniform_material(veng_shader shader, const char* name, veng_material *mat);

#endif
