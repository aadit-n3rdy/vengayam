#ifndef VENG_MESH_H
#define VENG_MESH_H

#define VENG_POSITION 1<<0;
#define VENG_NORMAL 1<<1;
#define VENG_TANGENT 1<<2;
#define VENG_TEXCOORD 1<<3;

typedef struct veng_mesh{
	unsigned int included_data;
	float *data;
	unsigned int data_count;
	int *indices;
	unsigned int index_count;
} veng_mesh;

#endif
