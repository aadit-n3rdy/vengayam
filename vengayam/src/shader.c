#include "shader.h"
#include "light.h"
#include "material.h"

#include <glad/glad.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <string.h>

#define VENG_SHADER_MAX_LEN 4096

uint32_t veng_compile_shader(const char *fname, uint32_t shader_type) {
	unsigned int ret = 0;
	char *buffer = malloc(VENG_SHADER_MAX_LEN);
	int len;
	unsigned int shader_id;
	FILE *fp  = fopen(fname, "r");
	if (fp) {
		len = fread(buffer, sizeof(char), 4096, fp);
		buffer[len] = '\0';
	} else {
		printf("ERROR: Could not open shader source\n");
		goto done;
	}
	shader_id = glCreateShader(shader_type);
	glShaderSource(shader_id, 1, (const GLchar *const *)&buffer, &len);
	glCompileShader(shader_id);
	int32_t status;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
	if (!status) {
		bzero(buffer, 2048);
		glGetShaderInfoLog(shader_id, 2048, NULL, buffer);
		printf("ERROR: Could not compile shader from file %s \n%s\n",
				fname, buffer);
		goto done;
	}
	ret = shader_id;
done:
	free(buffer);
	return ret;
}

veng_shader veng_shader_init(const char *vert_fname, const char *frag_fname) {
	uint32_t vert, frag;
	veng_shader ret;
	vert = veng_compile_shader(vert_fname, GL_VERTEX_SHADER);
	frag = veng_compile_shader(frag_fname, GL_FRAGMENT_SHADER);
	if (vert == 0 || frag == 0) {
		return 0;
	}
	ret = glCreateProgram();
	glAttachShader(ret, vert);
	glAttachShader(ret, frag);
	glLinkProgram(ret);
	int32_t status;
        glGetProgramiv(ret, GL_LINK_STATUS, &status);
        if (!status) {
                printf("ERROR: Could not link shader\n");
		ret = 0;
        }
	glDeleteShader(vert);
	glDeleteShader(frag);
	return ret;
}

int veng_shader_uniform_mat4(veng_shader shader, const char* name, float *val) {
	glUniformMatrix4fv(glGetUniformLocation(shader, name),
			1,
			GL_FALSE,
			val);
	return 0;
}

void veng_shader_bind(veng_shader shader) {
	glUseProgram(shader);
}

int veng_shader_uniform_vec3(veng_shader shader, const char* name, float *val) {
	glUniform3f(glGetUniformLocation(shader, name),
			val[0],
			val[1],
			val[2]);
	return 0;
}

int veng_shader_uniform_ui(veng_shader shader, const char* name, uint32_t val) {
	glUniform1ui(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int veng_shader_uniform_i(veng_shader shader, const char* name, int32_t val) {
	glUniform1i(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int veng_shader_uniform_f(veng_shader shader, const char* name, float val) {
	glUniform1f(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int veng_shader_uniform_point_light(veng_shader shader, const char* name, veng_point_light *light) {
	char names[4][64];
	for (int i = 0; i < 4; i++) {
		strcpy(names[i], name);
	}
	strcat(names[0], ".pos");
	strcat(names[1], ".diffuse_color");
	strcat(names[2], ".ambient_color");
	strcat(names[3], ".specular_color");
	veng_shader_uniform_vec3(shader, names[0], (float*)&light->pos);
	veng_shader_uniform_vec3(shader, names[1], (float*)&light->diffuse_color);
	veng_shader_uniform_vec3(shader, names[2], (float*)&light->ambient_color);
	veng_shader_uniform_vec3(shader, names[3], (float*)&light->specular_color);
	return 0;
}

int veng_shader_uniform_spot_light(veng_shader shader, const char *name, veng_spot_light *light) {
	char uname[64];
	sprintf(uname, "%s.pos", name);
	veng_shader_uniform_vec3(shader, uname, (float*)&light->pos);
	sprintf(uname, "%s.dir", name);
	veng_shader_uniform_vec3(shader, uname, (float*)&light->dir);
	sprintf(uname, "%s.inner", name);
	veng_shader_uniform_f(shader, uname, light->inner);
	sprintf(uname, "%s.outer", name);
	veng_shader_uniform_f(shader, uname, light->outer);
	sprintf(uname, "%s.diffuse_color", name);
	veng_shader_uniform_vec3(shader, uname, (float*)&light->diffuse_color);
	sprintf(uname, "%s.ambient_color", name);
	veng_shader_uniform_vec3(shader, uname, (float*)&light->ambient_color);
	sprintf(uname, "%s.specular_color", name);
	veng_shader_uniform_vec3(shader, uname, (float*)&light->specular_color);
	return 0;
}

int veng_shader_uniform_material(veng_shader shader, const char* name, veng_material *mat) {
	char names[5][20];
	for (int i = 0; i < 5; i++) {
		strcpy(names[i], name);
	}
	strcat(names[0], ".diffuse");
	strcat(names[1], ".specular");
	strcat(names[2], ".diffuse_tex");
	strcat(names[3], ".specular_tex");
	strcat(names[4], ".shininess");
	veng_shader_uniform_vec3(shader, names[0], (float*)&mat->diffuse);
	veng_shader_uniform_vec3(shader, names[1], (float*)&mat->specular);
	veng_shader_uniform_i(shader, names[2], mat->diffuse_tex_slot);
	veng_shader_uniform_i(shader, names[3], mat->specular_tex_slot);
	veng_shader_uniform_f(shader, names[4], mat->shininess);
	return 0;
}
