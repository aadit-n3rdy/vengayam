#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdio.h>

#include "matrix.h"
#include "vec.h"

int veng_multip_mat4 (mat4 m1, mat4 m2, mat4 result) {
	int r = 0;
	int c = 0;
	int i = 0;
	float sum = 0;
	int ret = 0;
	if (m1 == result || m2 == result) {
		mat4 tmp;
		ret = veng_multip_mat4(m1, m2, tmp);
		memcpy(result, tmp, sizeof(mat4));
		goto done;
	} else {
		for (r = 0; r < 4; r++) {
			for (c = 0; c < 4; c++) {
				sum = 0;
				for (i = 0; i < 4; i++) {
					sum += m1[r][i] * m2[i][c];
				}
				result[r][c] = sum;
			}
		}
	}
done:
	return ret;
	
}

vec4 veng_multip_mat4_vec4(mat4 m, vec4 v) {
	int r = 0;
	int i = 0;
	float sum = 0;
	float *vec = (float*)&v;
	float result[4] = {0, 0, 0, 0};
	for (i = 0; r < 4; r++) {
		for (i = 0; i < 4; i++) {
			sum += m[r][i] + vec[i];
		}
		result[i] = sum;
	}
	return (vec4){result[0], result[1], result[2], result[3]};
}

int veng_translate_mat4(vec3 v, mat4 dest) {
	dest[0][0] = 1;
	dest[0][1] = 0;
	dest[0][2] = 0;
	dest[0][3] = 0;
	dest[1][0] = 0;
	dest[1][1] = 1;
	dest[1][2] = 0;
	dest[1][3] = 0;
	dest[2][0] = 0;
	dest[2][1] = 0;
	dest[2][2] = 1;
	dest[2][3] = 0;
	dest[3][0] = v.x;
	dest[3][1] = v.y;
	dest[3][2] = v.z;
	dest[3][3] = 1;
	return 0;
}

int veng_scale_mat4(vec3 v, mat4 dest) {
	bzero((float*)dest, sizeof(mat4));
	dest[0][0] = v.x;
	dest[1][1] = v.y;
	dest[2][2] = v.z;
	dest[3][3] = 1;
	return 0;
}

int veng_persp_mat4(float near, float far, float fov, float ratio, mat4 dest) {
	float tinv = 1/tanf(fov/2.0f);
	bzero((float*)dest, sizeof(mat4));
	dest[0][0] = tinv/ratio;
	dest[1][1] = tinv;
	dest[2][2] = (far+near)/(near-far);
	dest[2][3] = -1;
	dest[3][2] = 2*near*far/(near-far);
	return 0;
};

int veng_rot_mat4(vec3 v, mat4 dest) {
	float sinx = sinf(v.x);
	float cosx = cosf(v.x);
	float siny = sinf(v.y);
	float cosy = cosf(v.y);
	float sinz = sinf(v.z);
	float cosz = cosf(v.z);
	mat4 rotx_raw = {
		{1, 0, 0, 0},
		{0, cosx, sinx, 0},
		{0, -sinx, cosx, 0},
		{0, 0, 0, 1}
	};
	mat4 roty_raw = {
		{cosy, 0, -siny, 0},
		{0, 1, 0, 0},
		{siny, 0, cosy, 0},
		{0, 0, 0, 1}
	};
	mat4 rotz_raw = {
		{cosz, sinz, 0, 0},
		{-sinz, cosz, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	};
	veng_multip_mat4(roty_raw, rotx_raw, dest);
	veng_multip_mat4(rotz_raw, dest, dest);
	return 0;
}

int veng_print_mat4(mat4 m) {
	int i = 0;
	int j = 0;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			printf("%10f ", m[i][j]);
		}
		printf("\n");
	}
	return 0;
}

int veng_lookat_mat4(vec3 cam, vec3 front, mat4 result) {
	front = veng_multip_vec3_float(veng_normalise_vec3(front), -1.0f);
	vec3 up = {0.0f, 1.0f, 0.0f};
	vec3 right = veng_normalise_vec3(veng_cross_vec3(up, front));
	up = veng_normalise_vec3(veng_cross_vec3(front, right));
	mat4 rotate = {
		{right.x, up.x, front.x, 0.0f},
		{right.y, up.y, front.y, 0.0f},
		{right.z, up.z, front.z, 0.0f},
		{0, 0, 0, 1}
	};
	mat4 translate;
	veng_translate_mat4(veng_multip_vec3_float(cam, -1), translate);
	veng_multip_mat4(translate, rotate, result);
	return 0;
}
