#include "texture.h"
#include "stb_image.h"

#include <glad/glad.h>

#include <stdint.h>

int veng_gen_tex(const char* path, int channels, veng_tex *tex, bool srgb ) {
	glGenTextures(1, &tex->id);
	unsigned char *data = stbi_load(path, 
			&tex->width, &tex->height, &tex->channels, channels);
	if (!data) {
		printf("ERROR: Could not load texture\n");
		return -1;
	}
	glGenerateMipmap(GL_TEXTURE_2D);
	veng_gen_tex_raw((const char *)data, tex->width, tex->height, channels, tex, srgb);
	stbi_image_free(data);
	return 0;
}

int veng_bind_tex(veng_tex *tex, uint32_t slot) {
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, tex->id);
	return 0;
}

int veng_gen_tex_raw(const char *data, int width, int height, int channels, veng_tex *tex, bool srgb) {
	glGenTextures(1, &tex->id);
	glBindTexture(GL_TEXTURE_2D, tex->id);
	GLenum format = GL_RGB;
	GLenum internal_format = GL_RGB;
	if (channels == 1) {
		format = GL_RED;
		internal_format = format;
	} else if (channels == 2) {
		format = GL_RED;
	internal_format = format;

	} else if (channels == 3) {
		format = GL_RGB;
		internal_format = format;
		if (srgb) {
			internal_format = GL_SRGB;
		}
	} else if (channels == 4) {
		format = GL_RGBA;
		internal_format = format;
	}
	tex->width = width;
	tex->height = height;
	tex->channels = channels;
	glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	return 0;
}
