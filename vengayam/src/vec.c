#include "vec.h"

#include <math.h>

vec3 veng_cross_vec3 (vec3 v1, vec3 v2) {
	vec3 ret;
	ret.x = v1.y * v2.z - v2.y * v1.z;
	ret.y = v1.z * v2.x - v1.x * v2.z;
	ret.z = v1.x * v2.y - v1.y * v2.x;
	return ret;
}

float veng_dot_vec3 (vec3 v1, vec3 v2) {
	return v1.x*v2.x + v1.y+v2.y + v1.z+v2.z;
}

vec3 veng_normalise_vec3 (vec3 v) {
	vec3 result;
	if (v.x == 0 && v.y == 0 && v.z == 0) {
		return v;
	}
	float mag = 1.0f/sqrtf(v.x*v.x +
			v.y * v.y +
			v.z * v.z);
	result.x = v.x * mag;
	result.y = v.y * mag;
	result.z = v.z * mag;
	return result;
}

vec3 veng_multip_vec3_float(vec3 v, float f) {
	vec3 result = {v.x * f,
		v.y * f,
		v.z * f};
	return result;
}

vec3 veng_add_vec3(vec3 v1, vec3 v2) {
	vec3 result = { v1.x + v2.x,
		v1.y + v2.y,
		v1.z + v2.z};
	return result;
}

vec4 veng_normalise_vec4(vec4 v) {
	vec4 result;
	float mag = 1.0f/sqrtf(v.x*v.x +
			v.y * v.y +
			v.z * v.z);
	result.x = v.x * mag;
	result.y = v.y * mag;
	result.z = v.z * mag;
	result.w = v.w;
	return result;
}
