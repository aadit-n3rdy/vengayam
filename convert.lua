json = require "json"
inspect = require "inspect"

infile = io.open("monke.gltf", "r")
io.input(infile)
gltf_str = infile:read("*all")
gltf = json.decode(gltf_str)
meshes = gltf["meshes"]
io.write("Enter mesh to load: ")
mesh_name = io.stdin:read()
mesh = {}
for _, i in ipairs(meshes)
do
	if i["name"] == mesh_name then
		mesh = i
		break
	end
end

mesh = mesh["primitives"][1]
index_acc = mesh["indices"]
attribs = mesh["attributes"]
pos_acc = attribs["POSITION"]
normal_acc = attribs["NORMAL"]
tex_acc = attribs["TEXCOORD_0"]

acc = gltf["accessors"]
pos_acc = acc[pos_acc+1]
normal_acc = acc[normal_acc+1]
tex_acc = acc[tex_acc+1]
